import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { UserRegisterComponent } from './user/user-register/user-register.component';
import{Routes,RouterModule} from '@angular/router';
import { UserLoginComponent } from './user-login/user-login.component';

const appRoutes: Routes =[
  {path: 'user/register', component:UserRegisterComponent}
]
@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    UserRegisterComponent,
    UserLoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
